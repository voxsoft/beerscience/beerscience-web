import React, { useContext, useEffect } from 'react';
import ModalDialog from './ModalDialog';
import ModalContext from '../../context/modalContext';
import ModalListContext from '../../context/modalListContext';

const ModalContainer = () => {
    const {modals, closeModal} = useContext<any>(ModalListContext);

    useEffect(() => {
        if (modals.length > 0) {
            if (!document.body.classList.contains('modal-opened')) {
                document.body.classList.add('modal-opened');
            }
        } else {
            document.body.classList.remove('modal-opened');
        }
    }, [modals.length]);
    
    return modals && modals.length > 0 ? (
        <div className='fixed overflow-auto top-0 left-0 h-full w-full bg-black bg-opacity-50 z-50'>
            {modals.map((modal: any) => <ModalContext.Provider value={modal} key={'modal' + modal.id}>
                    <ModalDialog closeDialog={() => closeModal(modal.id)} width={modal.width} />
                </ModalContext.Provider>)}
        </div>
    ) : null;
}

export default ModalContainer;
