import React, { useContext, useEffect, useRef } from 'react';
import ModalContext from '../../context/modalContext';

function useClickOutsideListener(ref: any, closeDialog: Function) {
    useEffect(() => {
        function handleClickOutside(event: any) {
          if (ref.current && !ref.current.contains(event.target)) {
            closeDialog();
          }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
          document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref, closeDialog]);
}

const ModalDialog = ({closeDialog, width}: {closeDialog: Function, width?: string}) => {
    const {content} = useContext<any>(ModalContext);
    const overlayRef = useRef(null);
    useClickOutsideListener(overlayRef, closeDialog);

    const close = () => {
        closeDialog();
    }

    const w = width === 'sm' ? ' md:w-[400px] md:left-[calc(50%-200px)]' : ' xl:w-[50%] xl:left-[25%]';
    return (
      <div ref={overlayRef} className={`absolute w-[320px] left-[calc(50%-160px)] top-[60px] md:top-[10%] 
      bg-white z-50` + w}>
        <div className='w-full h-full'>
            <div className='absolute text-3xl right-4 top-2 cursor-pointer' onClick={close}>&times;</div>
            {content}
        </div>
      </div>
    );
}

export default ModalDialog;
