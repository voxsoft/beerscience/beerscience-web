import { createContext } from "react";
import { TestedBeerInterface } from "../models/TestedBeerModel";
// import ReactDOM from "react-dom/client";

const BeersContext = createContext<TestedBeerInterface | null>(null);

export default BeersContext;