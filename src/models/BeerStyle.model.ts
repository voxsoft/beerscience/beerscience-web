export interface BeerStyle {
    id: number;
    name: string;
    description: string;
    minABV?: number;
    maxABV?: number;
    minIBU?: number;
    maxIBU?: number;
    shortDescription?: string;
    moderated?: boolean;
}