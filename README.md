# BeerScience frontend
A simple frontend application to experiment with NodeJS deploy

### Local environment
- Copy `.env.example` to `.env` and change values if needed
- Run `docker-compose up` in root directory of backend (This will take tine)
- Go to [http://localhost:3000](http://localhost:3000) to query backend
