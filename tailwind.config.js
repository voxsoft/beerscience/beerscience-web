/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/components/**/*.{js,jsx,ts,tsx}",
    "./src/components/layout/*.{js,jsx,ts,tsx}",
    "./components/*.{js,jsx,ts,tsx}",
    "./src/pages/*.{js,jsx,ts,tsx}",
    "./pages/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'black-blue': '#06090b',
        'dark-blue': '#18222c',
        'medium-blue': '#24415a',
        'light-blue': '#466077',
        'pale-blue': '#95a2be',
        'beer-blue-300': '#7fbbf1',
        'dark-yellow': '#f7d15f',
        'yellow': '#fdf29f',
        'light-yellow': '#e5e2c9',
        'lightest-yellow': '#f5f5e9',
        'warm-yellow': '#fcefcf'
      },
      boxShadow: {
        'outline': '0 0 2px 1px',
      }
    },
  },
  plugins: [],
}

